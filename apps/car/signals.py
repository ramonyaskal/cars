from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import Car


@receiver(pre_save, sender=Car)
def car_pre_save(sender, instance, **kwargs):
    instance.clean()

    # Set Category by year
    instance.category = instance.get_car_category_by_year()

from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.validators import MinValueValidator
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from car.utils import max_value_current_year


class BaseModel(models.Model):
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Brand(BaseModel):
    name = models.CharField(max_length=250)

    class Meta:
        verbose_name = _('Brand')
        ordering = ['name']

    def __str__(self):
        return self.name


class CarModel(BaseModel):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, related_name='car_models')
    name = models.CharField(max_length=250)

    class Meta:
        verbose_name = _('Car Model')
        ordering = ['brand', 'name']

    def __str__(self):
        return f"{self.brand} {self.name}"


class UserAccountManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError('Username must be set!')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username_):
        return self.get(username=username_)


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserAccountManager()
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(_('email'))
    first_name = models.CharField(_('First name'), max_length=255)
    last_name = models.CharField(_('Last name'), max_length=255)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = _('User')
        ordering = ['username', ]

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return f"{self.full_name} {self.email}"


class Car(BaseModel):
    CATEGORY_AUTO = 0
    CATEGORY_BEFORE_1990 = 1
    CATEGORY_1900_TO_2000 = 2
    CATEGORY_2000_TO_2010 = 3
    CATEGORY_AFTER_2010 = 4

    CATEGORY_CHOICES = (
        (CATEGORY_AUTO, _("Automatic set")),
        (CATEGORY_BEFORE_1990, _("Before 1990 release")),
        (CATEGORY_1900_TO_2000, _("From 1990 to 2000 release")),
        (CATEGORY_2000_TO_2010, _("From 2000 to 2010 release")),
        (CATEGORY_AFTER_2010, _("After 2010 release")),
    )

    car_model = models.ForeignKey(CarModel, on_delete=models.CASCADE, related_name='cars')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    year = models.IntegerField(_('year'), validators=[MinValueValidator(1800), max_value_current_year])
    category = models.PositiveIntegerField(_("Category"), choices=CATEGORY_CHOICES,
                                           default=CATEGORY_AUTO, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Car')
        ordering = ['user', 'car_model']

    def __str__(self):
        return f"<{self.__class__.__name__}: {self.id}> {self.car_model} {self.user}"

    @property
    def category_name(self):
        return self.CATEGORY_CHOICES[int(self.category)][1]

    def get_car_category_by_year(self):
        if self.year < 1990:
            return self.CATEGORY_BEFORE_1990
        elif 1990 <= self.year < 2000:
            return self.CATEGORY_1900_TO_2000
        elif 2000 <= self.year < 2010:
            return self.CATEGORY_2000_TO_2010
        elif self.year > 2010:
            return self.CATEGORY_AFTER_2010

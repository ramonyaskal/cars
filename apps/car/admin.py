from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseAdmin

from car import models


@admin.register(models.Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'update', 'created')
    readonly_fields = ('update', 'created')
    search_fields = ('name',)


@admin.register(models.CarModel)
class CarModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand', 'update', 'created')
    readonly_fields = ('update', 'created')
    search_fields = ('name',)


@admin.register(models.User)
class UserAdmin(BaseAdmin):
    list_display = ('username', 'first_name', 'last_name')
    search_fields = ('username', 'first_name', 'last_name')


@admin.register(models.Car)
class CarAdmin(admin.ModelAdmin):
    list_display = ('car_model', 'price', 'category_name', 'year', 'update', 'created')
    readonly_fields = ('category', 'update', 'created')
    search_fields = (
        'car_model__name',
        'car_model__brand__name',
        'year',
        'price',
    )
    autocomplete_fields = ['user', 'car_model']

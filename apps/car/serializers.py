from rest_framework import serializers

from .models import Car, User, CarModel, Brand


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'full_name')


class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = ('id', 'name',)


class CarModelSerializer(serializers.ModelSerializer):
    brand = BrandSerializer(many=False, read_only=True)

    class Meta:
        model = CarModel
        fields = ('id', 'name', 'brand')


class CarSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False, read_only=True)
    car_model = CarModelSerializer(many=False, read_only=True)

    class Meta:
        model = Car
        fields = (
            'id',
            'user',
            'car_model',
            'price',
            'year',
            'category',
            'category_name',
        )
        datatables_always_serialize = ('id',)

from django.urls import path
from . import views


urlpatterns = [
    path(r'', views.CarsView.as_view(), name='cars'),
]

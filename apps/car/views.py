from django.views.generic import TemplateView
from rest_framework import viewsets

from car.models import Car
from car.serializers import CarSerializer


class CarsView(TemplateView):
    """
    View Main page with table cars
    """
    template_name = "cars.html"


class CarsApiViewSet(viewsets.ModelViewSet):
    """
    View api for datatable cars
    """
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    http_method_names = ['get', 'head']

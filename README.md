**Test task**

Create cars data table.
See full task in [this file](https://bitbucket.org/ramonyaskal/cars/downloads/task.pdf).

---

## Install


I. Clone this **Repository**.
```
git clone https://ramonyaskal@bitbucket.org/ramonyaskal/cars.git
```

II. Create **Virtual Environment** in project folder.
```
python3 -m venv venv
```

III. Activate **Virtual Environment**.
```
source venv/bin/activate
```

IV. Install **libs**.
```
pip install -r requirements/base.txt
```

V. Create tables in **db**.
```
python manage.py migrate
```

VI. Load **Mock Data** to db.
```
python manage.py loaddata apps/car/fixtures/initial_all_data.json
```

VII. Create **Superuser** account.
```
python manage.py createsuperuser
```

VIII. **Run**
```
python manage.py runserver
```

IX. Open in browser test page [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

---

## Author

Roman Yaskal

If you have any question please write me 

[skype](https://join.skype.com/invite/p9teFE5nycK5)

[facebook](https://www.facebook.com/roman.yaskal)

[ramonsobaka@gmail.com](ramonsobaka@gmail.com)